clear all 
close all 
clc 

global RE MU ve 

RE = 6371; %km
m0 = 30e3; 
MU = 4e5; 
ve = 15; 

%% NMPC design 

par.model = @launch_model; 
par.n = 7; 
par.Ts = 50; 
par.Tp = 450; 
par.R = 0.1*eye(3); 
par.P = diag([10,1e6*ones(1,4)]); 
pat.tol = [5;0.01*ones(4,1)]'; 

K = nmpc_design(par); 

%% simulation 

%initial reference 
r0 = [RE 0 0]'; 
v0 = [0 0.465 0]';

x0=[r0;v0;m0]';
u0 = zeros(3,1); 

%reference 
ar = RE+500; 
er = zeros(3,1);
cir = 1; 
yr = [ar;er;cir]; 

% sim
Tsim = 3600; 
st_size = 1; 

open('sim_launch')
% orbit_animation(x0,[0,90],7.5e3)
sim ('sim_launch')

