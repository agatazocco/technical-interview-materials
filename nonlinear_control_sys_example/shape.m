clear all 
close all 
clc 

global MU RE ve 

RE = 6371; 
MU = 4e5; 
ve = 4.4; 
m0 = 10e3; 

%% NMPC DESIGN 

par.model = @change_shape;
par.n =7;
par.Ts = 30; 
par.Tp = 90; 
par.R = 0.1*eye(3);
% par.Q = zeros(5);
par.P = diag([1,1e5*ones(1,4)]); 
%par.tol = [1, 0.005*ones(1,4)]'; 
par.ub = 132*ones(3,1); 
par.lb = -132*ones(3,1); 
par.tol=[1;0.005*ones(4,1)];
par.Tctrl=500;

K = nmpc_design(par); 

%% simulation 

% initial conditions 

r0=6871;
v0 = sqrt(MU/r0); 
x0 = [r0 0 0 0 v0 0 m0]'; 
u0 = [0;0;0]; 

% reference 
ar=8932;
er = [0.25;0;0]; 
cir = 1; 
yr = [ar;er;cir];

% simulation
Tsim = 30000; 
st_size = 2; 
open('sim_shape')
sim ('sim_shape')
