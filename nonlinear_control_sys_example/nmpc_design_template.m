clear all 
close all 
clc

%% NMPC Design

par.model=@model;       % Matlab function with state and output equations
par.nlc=@constraints;   % Matlab function with constraints
par.n=...               % System order
par.Ts=...              % NMPC sampling time
par.Tp=...              % NMPC prediction horizon
par.R=...               % R matrix 
par.P=...               % P matrix 
par.Q=...               % Q matrix 
par.lb=...              % Command input upper saturation 
par.ub=...              % Command input lower saturation 
par.tol=...             % Tolerances on the tracking errors
par.Tctrl=...           % NMPC switch-on time

K=nmpc_design(par);

