
/*
*********************************************************************************************************
*
*                                             uC/OS-III
*
* Filename : main.c
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                            INCLUDE FILES
*********************************************************************************************************
*/

#include  <cpu.h>
#include  <lib_mem.h>
#include  <os.h>
#include  <bsp_os.h>
#include  <bsp_clk.h>
#include  <bsp_int.h>
#include  <bsp_led.h>

#include  "os_app_hooks.h"
#include  "../app_cfg.h"

#include  <bsp_switch.h>
#include  <bsp_adc.h>
#include  <bsp_timer.h>
/*
*********************************************************************************************************
*                                            LOCAL DEFINES
*********************************************************************************************************
*/

#define SET_DUTY_CYCLE(color, duty_cycle) FTM0->CONTROLS[color].CnV=FTM_CnV_VAL(duty_cycle)

/*
*********************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/

static  OS_TCB   StartupTaskTCB;
static  CPU_STK  StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE];


extern CPU_INT08U SW2_status;
extern CPU_INT08U cnt;

OS_SEM ADC0sem;
extern CPU_INT16U ADC0_adc_chx;

CPU_INT16U adcResultInMv_pot;
volatile CPU_INT16U duty_cycle = 0;



/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static  void  StartupTask (void  *p_arg);

void BSP_ADC0_convertAdcChan_interrupt(CPU_INT16U adcChan);
static void SetColor (CPU_INT08U cnt, CPU_INT16U duty_cycle);


/*
*********************************************************************************************************
*                                                main()
*
* Description : This is the standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary initialization.
*
* Arguments   : none
*
* Returns     : none
*
* Notes       : none
*********************************************************************************************************
*/




int  main (void)
{
    OS_ERR  os_err;


    BSP_ClkInit();                             /* Initialize the main clock.                           */
    BSP_IntInit();
    BSP_OS_TickInit();                         /* Initialize kernel tick timer                         */

    Mem_Init();                                /* Initialize Memory Management Module                   */
    CPU_IntDis();                              /* Disable all Interrupts.                              */
    CPU_Init();                                /* Initialize the uC/CPU services                       */

    OSInit(&os_err);                           /* Initialize uC/OS-III                                 */
    if (os_err != OS_ERR_NONE) {
        while (1);
    }

    App_OS_SetAllHooks();                      /* Set all applications hooks                           */


    OSSemCreate(&ADC0sem, "ADC0 Semaphore", 0u, &os_err);


    OSTaskCreate(&StartupTaskTCB,              /* Create the startup task                              */
                 "Startup Task",
                  StartupTask,
                  0u,
                  APP_CFG_STARTUP_TASK_PRIO,
                 &StartupTaskStk[0u],
                  StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE / 10u],
                  APP_CFG_STARTUP_TASK_STK_SIZE,
                  0u,
                  0u,
                  0u,
                 (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 &os_err);
    if (os_err != OS_ERR_NONE) {
        while (1);
    }

    OSStart(&os_err);                          /* Start multitasking (i.e. give control to uC/OS-III)  */

    while (DEF_ON) {                           /* Should Never Get Here.                               */
        ;
    }
}


/*
*********************************************************************************************************
*                                            STARTUP TASK
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
*
* Arguments   : p_arg   is the argument passed to 'StartupTask()' by 'OSTaskCreate()'.
*
* Returns     : none
*
* Notes       : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

//AppTaskStart

static  void  StartupTask (void *p_arg){
  
    OS_ERR      os_err;
    CPU_INT08U  rgb_led_color;


   (void)p_arg;

    OS_TRACE_INIT();                           /* Initialize Trace recorder                            */

    BSP_OS_TickEnable();                       /* Enable the tick timer and interrupt                  */

#if OS_CFG_STAT_TASK_EN > 0u
    OSStatTaskCPUUsageInit(&os_err);           /* Compute CPU capacity with no task running            */
#endif

#ifdef CPU_CFG_INT_DIS_MEAS_EN
    CPU_IntDisMeasMaxCurReset();
#endif

    BSP_Switch_Init();
    BSP_ADC0_init_interrupt();
    BSP_FTM0_PWM_Init ();


while (DEF_TRUE) {                         /* Task body, always written as an infinite loop        */
    	                                      /* Cycle through the different colors in the RGB LED     */

    	BSP_ADC0_convertAdcChan_interrupt(12);

    	OSSemPend(&ADC0sem, 0u, OS_OPT_PEND_BLOCKING, 0u, &os_err);

    	adcResultInMv_pot = ADC0_adc_chx;

    	duty_cycle = (adcResultInMv_pot*7999)/5000;

    	SetColor(cnt, duty_cycle);
    }
}


static void SetColor (CPU_INT08U cnt, CPU_INT16U duty_cycle) {

	FTM0->SC = 0;

	int i = 0;

	for (i = 0; i<3; i++) {

		if(i==(int)cnt)
			SET_DUTY_CYCLE(i,duty_cycle);
		else
			SET_DUTY_CYCLE(i, 7999);

		FTM0->CNT = 0;

	}

	FTM0->SC = FTM_SC_CLKS(1) | FTM_SC_PWMEN2_MASK | FTM_SC_PWMEN1_MASK | FTM_SC_PWMEN0_MASK;
}
