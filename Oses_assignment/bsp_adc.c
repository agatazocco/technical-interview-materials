#include <lib_def.h>
#include <cpu.h>
#include <bsp_int.h>
#include <os.h>
#include "S32K144.h"
#include  <lib_mem.h>

#include  <bsp_os.h>
#include  <bsp_clk.h>
#include  <bsp_int.h>
#include  "os_app_hooks.h"
#include  "../app_cfg.h"
#include <bsp_adc.h>

volatile CPU_INT16U ADC0_adc_chx;
extern  OS_SEM ADC0sem;


void BSP_ADC0_init_interrupt(void)
{

	PCC->PCCn[PCC_ADC0_INDEX] &=~ PCC_PCCn_CGC_MASK; 						/* Disable clock to change PCS */

	PCC->PCCn[PCC_ADC0_INDEX] |= PCC_PCCn_PCS(1); 							/* PCS=1: Select SOSCDIV2 */

	PCC->PCCn[PCC_ADC0_INDEX] |= PCC_PCCn_CGC_MASK;					 		/* Enable bus clock in ADC */

	ADC0->SC1[0] =0x00007F; 												/* ADCH=1F: Module is disabled for conversions*/
																			/* AIEN=1: Interrupts are disabled */

	ADC0->CFG1 = 0x000000004; 												/* ADICLK=0: Input clk=ALTCLK1=SOSCDIV2 */
																			/* ADIV=0: Prescaler=1 */
																			/* MODE=1: 12-bit conversion */

	ADC0->CFG2 = 0x00000000C; /* SMPLTS=12(default): sample time is 13 ADC clks */

	ADC0->SC2 = 0x00000000; /* ADTRG=0: SW trigger */
																			/* ACFE,ACFGT,ACREN=0: Compare func disabled */
																			/* DMAEN=0: DMA disabled */
																			/* REFSEL=0: Voltage reference pins= VREFH, VREEFL */

	ADC0->SC3 = 0x00000000; 												/* CAL=0: Do not start calibration sequence */
																			/* ADCO=0: One conversion performed */
																			/* AVGE,AVGS=0: HW average function disabled */
																			/* Enable interrupt for ADC0 */


	BSP_IntVectSet(ADC0_IRQn, 0, 0, ADC0_IRQHandler);

	BSP_IntEnable(ADC0_IRQn);
}






void BSP_ADC0_convertAdcChan_interrupt(CPU_INT16U adcChan)
{


	/* For SW trigger mode, SC1[0] is used */

	ADC0->SC1[0]&=~ADC_SC1_ADCH_MASK; /* Clear prior ADCH bits */

	ADC0->SC1[0] = ADC_SC1_ADCH(adcChan) | 0x40; /* Initiate Conversion and AIEN=1*/
}


void ADC0_IRQHandler(void)
{

	CPU_INT16U adc_result=0;

	OS_ERR os_err;

	OSIntEnter();

	/* For SW trigger mode, R[0] is used */

	adc_result=ADC0->R[0];

	/* Convert result to mv for 0-5V range */

	ADC0_adc_chx = (CPU_INT16U) ((5000*adc_result)/0xFFF);

	ADC0->SC1[0] =0x00007F; /* Disable conversion & clear interrupt */

	OSSemPost(&ADC0sem, OS_OPT_POST_1 | OS_OPT_POST_NO_SCHED, &os_err);

	OSIntExit();
}
