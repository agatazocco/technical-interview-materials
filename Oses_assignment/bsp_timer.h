#ifndef BSP_TIMER_PRESENT
#define BSP_TIMER_PRESENT

#ifdef __cplusplus
extern "C" {
#endif


void BSP_FTM0_PWM_Init (void);
void BSP_FTM0_PWM_Set (void);

#ifdef __cplusplus
}
#endif

#endif
