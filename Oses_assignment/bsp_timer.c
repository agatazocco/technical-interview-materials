#include <lib_def.h>
#include <bsp_int.h>
#include <os.h>
#include "bsp_timer.h"
#include "S32K144.h"



extern duty_cycle;
extern cnt;


void BSP_FTM0_PWM_Init (void){

	PCC->PCCn[PCC_PORTD_INDEX] = PCC_PCCn_CGC_MASK;						/*Enable clock PORT D*/
	
	PCC->PCCn[PCC_FTM0_INDEX] = PCC_PCCn_PCS(6) | PCC_PCCn_CGC_MASK; 	/*Enable the clock for FTM0, which is associated to port D alt mode 2*/
	
	
	PORTD->PCR[15] = PORT_PCR_MUX(2); 									// Configure PD15 (RGB_RED) as a FTM0_CH0 function
	PORTD->PCR[16] = PORT_PCR_MUX(2);									// Configure PD16 (RGB_GREEN) as a FTM0_CH1 function
	PORTD->PCR[0] = PORT_PCR_MUX(2);									// Configure PD0 (RGB_BLUE) as a FTM0_CH2 function
	

	/* Enable registers updating from write buffers */
	FTM0->MODE = FTM_MODE_FTMEN_MASK;

	
	/* Set modulo to 10 KHz PWM frequency @ 80 MHz system clock with pre-scaler 1*/
	FTM0->MOD = FTM_MOD_MOD(8000-1);

	
	/* Set CNTIN in initialization stage */
	FTM0->CNTIN = FTM_CNTIN_INIT(0);

	
	/* Enable high-true pulses of PWM signals */
	FTM0->CONTROLS[0].CnSC = FTM_CnSC_MSB_MASK | FTM_CnSC_ELSB_MASK;
	FTM0->CONTROLS[1].CnSC = FTM_CnSC_MSB_MASK | FTM_CnSC_ELSB_MASK;
	FTM0->CONTROLS[2].CnSC = FTM_CnSC_MSB_MASK | FTM_CnSC_ELSB_MASK;
	
	/* Set channel value in initialization stage */
	FTM0->CONTROLS[0].CnV=FTM_CnV_VAL(7999);								 	 /* 100% duty cycle */
	FTM0->CONTROLS[1].CnV=FTM_CnV_VAL(0); 								 		 /* 0% duty cycle */
	FTM0->CONTROLS[2].CnV=FTM_CnV_VAL(0); 										 /* 0% duty cycle */

	/* Reset FTM counter */
	FTM0->CNT = 0;

	/* Clock selection and enabling PWM generation */
	FTM0->SC = FTM_SC_CLKS(1) | FTM_SC_PWMEN2_MASK | FTM_SC_PWMEN1_MASK | FTM_SC_PWMEN0_MASK;
}
