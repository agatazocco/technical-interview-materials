
#include <lib_def.h>
#include <cpu.h>
#include <bsp_int.h>
#include <os.h>
#include "S32K144.h"


#ifndef BSP_ADC_PRESENT
#define BSP_ADC_PRESENT

#ifdef __cplusplus
extern "C" {
#endif


void BSP_ADC0_init_interrupt(void);
void BSP_ADC0_convertAdcChan_interrupt(CPU_INT16U adcChan);
void ADC0_IRQHandler(void);

#ifdef __cplusplus
}
#endif

#endif
