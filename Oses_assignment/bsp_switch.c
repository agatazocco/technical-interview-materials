/*
*********************************************************************************************************
*
*										 MICRIUM BOARD SUPPORT PACKAGE
*
* Filename : bsp_switch.c
*********************************************************************************************************
*

*********************************************************************************************************
* INCLUDE FILES
*********************************************************************************************************
*/

#include <lib_def.h>
#include <cpu.h>
#include <bsp_int.h>
#include <os.h>
#include "bsp_switch.h"
#include "S32K144.h"

/*
*********************************************************************************************************
*												 DEFINES
*********************************************************************************************************
*/

volatile CPU_INT08U SW2_status = 0;
volatile CPU_INT08U cnt = 0;
/*
*********************************************************************************************************
* 											LOCAL FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static void SW2_int_hdlr( void );

/*
*********************************************************************************************************
**												 GLOBAL FUNCTIONS
*********************************************************************************************************
*/

/*
*********************************************************************************************************
* BSP_Switch_Init()
*
* Description : Initializes the required pins that control the switches.
*
* Argument(s) : none.
*
* Return(s) : none.
*
* Note(s) : none.
*********************************************************************************************************
*/
void BSP_Switch_Init (void)
{
	PCC->PCCn[PCC_PORTC_INDEX] = PCC_PCCn_CGC_MASK; 							/* Enable clock to PORT C */

	PTC->PDDR &= ~(DEF_BIT_12);  												/* Set PC12 (SW2) as an input pin */


	PORTC->PCR[12] = 0x100u; 													/* Configure PC12 as a GPIO function */


	PORTC->PCR[12] |= 0x00090000u; 												/* Configure PC12 for single edge interrupt */


	BSP_IntVectSet(PORTC_IRQn, 0,0, SW2_int_hdlr); 								/* Register Interrupt Handler */

	BSP_IntEnable( PORTC_IRQn ); 												/* Enable the interrupt for PORTC */
}

/*
*********************************************************************************************************
* BSP_Switch_Read()
*
* Description : Read the required pin that control the switches.
*
* Argument(s) : switch to be read.
*
* Return(s) : value of the switch (0 or 1).
*
* Note(s) : none.
*********************************************************************************************************
*/

CPU_INT08U BSP_Switch_Read (BSP_SWITCH sw){


	if (sw)
		return( ((PTC->PDIR) >> 12) & 0x01u);
	else
		return( 0x00u );

}




/*
*********************************************************************************************************
* SW3_int_hdlr()
*
* Description : Respond to SW3 interrupt (PORTCn).
*
* Argument(s) : switch to be read.
*
* Return(s) : value of the switch (0 or 1).
*
* Note(s) : none.
*********************************************************************************************************
*/
static void SW2_int_hdlr( void )
{

	uint32_t ifsr;

	//CPU_CRITICAL_ENTER();

	OSIntEnter();

	ifsr = (PORTC->PCR[12]) & 0x01000000;

	if( (ifsr) ){

		SW2_status = BSP_Switch_Read( SW2);
		PORTC->PCR[12] |= 0x01000000;


		if( (SW2_status)){
			if (cnt<2)
				cnt++;
			else cnt=0;
		}
		else
			return( 0x00u );


	}

	//CPU_CRITICAL_EXIT();

	OSIntExit();
}
