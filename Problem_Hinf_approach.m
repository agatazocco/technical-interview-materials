clear all
close all
clc

s = tf('s');

K = [9 16]; 
p1 = [0.55 1.05]; 
p2 = [1.9 3.1]; 

Kn = mean(K); 
p1n = mean(p1);
p2n = mean(p2); 
Gpn = Kn/(s*(1+s/p1n)*(1+s/p2n)); 
kp = dcgain(s*Gpn); 

Gs = 2; 
Ga = 0.38; 
Gr = 1; 
Gd = 1; 

%% SPECIFICATIONS

% s1
kd = 4; 	%static gain
Gf = 1/(kd*Gs);

% s2 ni = 0; %number of poles at the origin 
% reference
R0 = 1; 
er = 0.15;
S0er = er/(R0*kd); 

%actuator disturb
% s3 ni = 1; %number of poles at the origin 
Da0 = 5.5e-3; 
eda = 5.8; 
S0da = eda/(Da0*kp); 
				
Sstar = S0da; %sensitity function at zero, S(s) = s^(ni+pi)*Sstar(s);
				%sensitivity S(s) = 1/(1+L); 
% s4 
% plant disturb
edp = 3.6e-4; 
ap = 2e-2; 
wp = 0.02; 
Mlf = edp/ap; 
Mlfdb = db(Mlf); 

% s5 
% sensor disturb
eds = 1.25e-4; 
ws = 40; 
as = 0.1; 
Mhf = eds*Gs/as; 
Mhfdb = db(Mhf);

% s8 
% maximun overshoot 
shat = 0.12; 
zeta = abs(log(shat))/sqrt(pi^2+(log(shat))^2);

%% Sensitivity and complemantary sensitivity function maximum peak (derived from requirements)

Sp0 = 2*zeta*sqrt(2+4*zeta^2+2*sqrt(1+8*zeta^2))/(sqrt(1+8*zeta^2)+4*zeta^2-1);
Tp0 = 1/(2*zeta*sqrt(1-zeta^2)); 
Tp0db = db(Tp0); 
Sp0db = db(Sp0); 

% s6 rising time
tr = 2.5; 
wntr = (pi-acos(zeta))/(tr*sqrt(1-zeta^2)); 
wctr = wntr*sqrt(sqrt(1+4*zeta^4)-2*zeta^2); 

% s7 settling time
ts = 5; 
wnts = -log(0.05)/(ts*zeta);
wcts = wnts*sqrt(sqrt(1+4*zeta^4)-2*zeta^2);

%% 
wn = max(wnts,wntr); 
wc = max(wcts,wctr);

%% PROTOTYPE FUNCTIONS second order function

Sprot = zpk(s*(s+2*zeta*wn)/(s^2+2*zeta*wn*s+wn^2))
Tprot = 1-Sprot; 

wc_S = getGainCrossover(Sprot/s^2,1)
wc_T = getGainCrossover(Tprot,1)

% bodemag(Sprot,Tprot)

%% Weighting functions (tuning of the parameters)
a = 0.7*Sstar; 
w1 = 14*wp; 
w3 = 0.01; 
w2 = sqrt(Sp0*w1/(a*w3));
Wsinv = a*(s^2)*(1+s/w1)/((1+s/w3)*(1+1.414*s/w2+(s/w2)^2));
figure 
bodemag(Sprot,Wsinv)
x = xlim; 
hold on 
plot([x(1) wp], [Mlfdb Mlfdb],'k--'), hold on 
plot([wp wp], [Mlfdb Sp0db],'k--'), hold on 
plot([wp x(2)], [Sp0db Sp0db],'k--'), grid on 
legend('Sprot','Wsinv')

%% 
wt = 2.2*wc;
Wtinv = Tp0/(1+1.414*s/wt+(s/wt)^2); 

figure 
bodemag(Tprot,Wtinv)
x = xlim; 
hold on 
plot([x(1) ws], [Tp0db Tp0db],'k--'), hold on 
plot([ws ws], [Tp0db Mhfdb],'k--'), hold on 
plot([ws x(2)], [Mhfdb Mhfdb],'k--'), grid on 
legend ('Tprot','Wtinv')

%% 
omega = logspace(-3,2,200); 
figure
for k = 9:1:16
    for p1 = 0.55:0.25:1.05
        for p2 = 1.9:0.2:3.1
            Gp = k/(s*(1+s/p1)*(1+s/p2));
            err = Gp/Gpn-1; 
            [m,f] = bode(err,omega); 
            m = squeeze(m); 
            loglog(omega,m); 
            hold on 
        end 
    end
end

mf = ginput(15); 
%
magg = vpck(mf(:,2),mf(:,1)); 
Wa = fitmag(magg); 
[A,B,C,D] = unpck(Wa); 
[Z,P,K] = ss2zp(A,B,C,D); 

Wu = zpk(Z,P,K); 
save(Wu, Wu)

%%
%Wu = 1.0835*(s+0.3363)*(s+1.32)/((s+2.365)*(s+0.7084)); 

Ws = inv(Wsinv); 
Wt = inv(Wtinv); 

figure 
bodemag(Wt,Wu)

W1 = Ws; 
W2 = Wt; 

lambda = 0.001*wc; 
W1mod = minreal(W1*(s/(s+lambda))^2); 
W2mod = dcgain(2.4*W2); 

%%
P = Ga*Gpn*Gs*Gf;
[Am,Bm,Cm,Dm] = linmod('generalized_plant'); 
M = ltisys(Am,Bm,Cm,Dm); 
M = sderiv(M,2,[1/wt 1]);
M = sderiv(M,2,[1/wt 1]);
[gopt,Gcmod] = hinflmi(M,[1 1],0,1e-2,[0 0 0]); 
[Ac,Bc,Cc,Dc] = ltiss(Gcmod); 
Gcmod = ss(Ac,Bc,Cc,Dc); 


%%
Gcmod = zpk(Gcmod)
z = zero(Gcmod);
p = pole(Gcmod);

% high frequency pole and low frequency pole and zero can be canceled
Gc = minreal(Gcmod*(1-s/p(1))*(s-p(6))*(s-p(5))/(s-z(4))/s)

%%
Lmod = Gcmod*P; 
L = Gc*P;
figure 
nichols(Lmod,'b')
hold on
myngridst(Tp0,Sp0)
hold on 
nichols(L,'r')


%% step response plot 
ymax = kd+shat*kd; 
Tsim = 30; 
sim('transient')
figure
plot(y.time, y.data)
hold on
plot([tr tr],[0 kd],'r')
hold on 
plot([tr Tsim],[kd kd],'r')
hold on 
plot([0 Tsim],[ymax ymax],'r')
hold on 
plot([ts ts],[0 4.2],'k')
hold on
plot([ts Tsim],[4.2 4.2],'k')

%% ramp reference 
R0sim = R0;
Da0sim = 0; 
apsim = 0; 
wpsim = 0; 
assim = 0; 
wssim = 0; 
Tsim = 200;
sim('plant')
figure
plot(e.time,e.data)
hold on 
plot([0 Tsim], [er er],'r')

%% actuator disturb
R0sim = 0;
Da0sim = Da0; 
apsim = 0; 
wpsim = 0; 
assim = 0; 
wssim = 0; 
Tsim = 200;
sim('impianto')
figure
plot(y.time,y.data)
hold on 
plot([0 Tsim], [eda eda],'r')

%% plant disturb
R0sim = 0;
Da0sim = 0; 
apsim = ap; 
wpsim = wp; 
assim = 0; 
wssim = 0; 
Tsim = 200;
sim('impianto')
figure
plot(y.time,y.data)
hold on 
plot([0 Tsim], [edp edp],'r')

%% sensor disturb
R0sim = 0;
Da0sim = 0; 
apsim = 0; 
wpsim = 0; 
assim = as; 
wssim = ws; 
Tsim = 200;
sim('impianto')
figure
plot(y.time,y.data)
hold on 
plot([0 Tsim], [eds eds],'r')


%% ---- ROBUST STABILITY ----- %%
Sn = 1/(1+L); 
Tn = L/(1+L); 
omega = logspace(-3,2,200); 
rs = Wu*Tn; 
figure 
bodemag(inv(Wu),Tn)
figure 
bodemag(rs)
[rs,f] = bode(omega,rs);
rs = squeeze(rs); 
RS = norm(rs,inf)

%% ---- NOMINAL PERFORMANCE ----- %%
Sn = 1/(1+L); 
Tn = L/(1+L); 
omega = logspace(-3,2,200); 
np = Wt*Tn; 
figure 
bodemag(Wtinv,Tn)
figure 
bodemag(np)
[np,f] = bode(omega,np);
np = squeeze(np); 
NP = norm(np,inf)

%% MU ANALISI S3
Wmu = 1/(s*S0da);

k = [9 16]; 
p1 = [0.55 1.05]; 
p2 = [1.9 3.1]; 

kn = mean(k); 
p1n = mean(p1n); 
p2n = mean(p2n); 

Wk = (k(2)-k(1))/(k(1)+k(2)); 
Wp1 = (p1(2)-p1(1))/(p1(1)+p1(2));
Wp2 = (p2(2)-p2(1))/(p2(1)+p2(2));

deltaset = [1 1; -1 0;-1 0;-1 0]; 
omega = logspace(-5,-3,150); 
[An,Bn,Cn,Dn] = linmod('S3_p5'); 
N = pck(An,Bn,Cn,Dn); 
Nf = frsp(N,omega);
mubnds = mu(Nf,deltaset);
vplot('liv,m',mubnds)


%% MU ANALISI S4
Wmu = 1/(Mlf);

k = [9 16]; 
p1 = [0.55 1.05]; 
p2 = [1.9 3.1]; 

kn = mean(k); 
p1n = mean(p1n); 
p2n = mean(p2n); 

Wk = (k(2)-k(1))/(k(1)+k(2)); 
Wp1 = (p1(2)-p1(1))/(p1(1)+p1(2));
Wp2 = (p2(2)-p2(1))/(p2(1)+p2(2));

deltaset = [1 1; -1 0;-1 0;-1 0]; 
omega = linspace(1e-5,wp); 
[An,Bn,Cn,Dn] = linmod('S3_p5'); 
N = pck(An,Bn,Cn,Dn); 
Nf = frsp(N,omega);
mubnds = mu(Nf,deltaset);
vplot('iv,m',mubnds)

%% MU stability analysis
k = [7 18]; 
p1 = [0.45 1.15]; 
p2 = [1.5 3.5]; 

kn = mean(k); 
p1n = mean(p1n); 
p2n = mean(p2n); 

Wk = (k(2)-k(1))/(k(1)+k(2)); 
Wp1 = (p1(2)-p1(1))/(p1(1)+p1(2));
Wp2 = (p2(2)-p2(1))/(p2(1)+p2(2));
deltaset = [-1 0;-1 0;-1 0]; 
omega = logspace(-3,2); 
[An,Bn,Cn,Dn] = linmod('stab'); 
N = pck(An,Bn,Cn,Dn); 
Nf = frsp(N,omega);
mubnds = mu(Nf,deltaset);
vplot('liv,m',mubnds)
