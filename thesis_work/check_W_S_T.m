
%% ----------------------------ROLL---------------------------------------- %% 

Kroll = minreal(Ksc(1,1));
L_roll = Kroll*P_SC(1,1); 
Sroll = 1/(1+L_roll); 
Troll = 1-Sroll;

W3inv = inv(W3);
wc_roll = getGainCrossover(L_roll,1)
wc_des = (getGainCrossover(W1(1,1),1)+getGainCrossover(W3(1,1),1))/2; 

figure 
bodemag(W1(1,1),'b--',inv(W3(1,1)),'r--')
hold on
bodemag(L_roll,'k')
hold on 

if isempty(wc_roll)
    plot ([1e-6 1e2], [1 1])
else
    plot([wc_roll wc_roll],[1 1],'*')
end 
hold on

if isempty(wc_des)
    plot ([1e-6 1e2], [1 1])
else
    plot([wc_des wc_des],[1 1],'o')
end 
grid on 
title('ROLL')
legend('W1','W3inv')

figure 
bodemag(Troll,'b',inv(W3(1,1)),'r')
legend('Troll','W3inv')

figure 
bodemag(Sroll,'b',inv(W1(1,1)),'r')
legend('Sroll','W1inv')

%% ----------------------------PITCH---------------------------------------- %% 

Kpitch = minreal(Ksc(2,2)); 
L_pitch = Kpitch*P_SC(2,2);
Spitch = 1/(1+L_pitch); 
Tpitch = 1-Spitch;

wc_des = (getGainCrossover(W1_efc(2,2),1)+getGainCrossover(W3(2,2),1))/2
wc_pitch = getGainCrossover(L_pitch,1)
 
figure 
bodemag(W1(2,2),'b--',inv(W3(2,2)),'r--')
hold on
bodemag(L_pitch,'k')

if isempty(wc_pitch)
    plot ([1e-6 1e2], [0 0])
else
    plot([wc_pitch wc_pitch],[1 1],'gp')
end 
hold on 
if isempty(wc_des)
    plot ([1e-6 1e2], [1 1])
else
    plot([wc_des wc_des],[1 1],'o')
end 
grid on 
title('PITCH')
legend('W1com','W3inv')


figure 
bodemag(Tpitch,'b',inv(W3(1,1)),'r')
legend('Tpitch','W3inv')

figure 
bodemag(Spitch,'b',inv(W1_efc(1,1)),'r')
legend('Spitch','W1inv')


%% ----------------------------YAW---------------------------------------- %% 

Kyaw = minreal(Ksc(3,3)); 
L_yaw = Kyaw*P_SC(3,3);  
Syaw = 1/(1+L_yaw);
Tyaw = 1-Syaw;

wc_des = (getGainCrossover(W1_efc(3,3),1)+getGainCrossover(W3(3,3),1))/2; 
wc_yaw = getGainCrossover(L_yaw,1)

figure 
bodemag(W1(3,3),'b--',inv(W3(3,3)),'r--')
hold on
bodemag(L_yaw,'k')
if isempty(wc_yaw)
    plot ([1e-6 1e2], [1 1])
else
    plot([wc_yaw wc_yaw],[1 1],'gp')
end 
hold on ,grid on 
title('YAW')
legend('W1com','W3inv')

figure 
bodemag(Tyaw,'b',inv(W3(1,1)),'r')
legend('Tyaw','W3inv')

figure 
bodemag(Syaw,'b',inv(W1_efc(1,1)),'r')
legend('Syaw','W3inv')

return