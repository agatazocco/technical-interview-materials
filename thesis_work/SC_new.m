
 %% ---------------- SPACECRAFT ATTITUDE CONTROL ------------------ %%
clear all 
close all 
clc 

s = tf('s'); 
fp = logspace(-6,1,200); 
wi = 2*pi*fp; 

%% Requirements
B80=fcn(s);
 

%% Noise 
Tsn=0.1;                                            % Noise generator sampling time [s]
lisa_noises

%% Plant
load LISA_plant_end                                 %tf at the end of cycle
load LISA_plant_begin                               %tf at the begin of cycle
P_end(:,:) = tf(LISA_plant_end(1:3,4:6));           
P_begin(:,:) = tf(LISA_plant_begin(1:3,4:6));           




%% ------------------------------ W1 ----------------------------------- %%
 %% ----to account for reference tracking and disturbance rejection ---%%

W1_end = minreal(fcn(requirements and noise tf));    %weighting functions formulation, W1 is a matrix
W1_begin = minreal(requirements and noise tf);

for i = 1:3
    for j = 1:3 
    figure 
    bodemag(W1_end(i,j),W1_begin(i,j)),hold on
    legend('W1_efc','W1_boc')
    end
end

%% ------------------------------ W2 ----------------------------------- %%
 %% --------------to account for command activity ---------------%%

W2 = 1e-5;

%% ------------------------------ W3 ----------------------------------- %%
 %% -------------- to account for sensor noise attenuation ------------%%

W3 = minreal(Fy.*Sreq)*eye(3); 


%% ---------------------------------------------------------------- %%

wc = (getGainCrossover(W1_begin(1,1),1)+getGainCrossover(W3(1,1),1))/2; 
delta = 0.0001*wc; 
W1_delta = minreal(W1.*(s/(s+delta))^2);
W1 = minreal(W1_begin*(s/(s+delta))^2);


N1 = 100;
N3 = 1;
W1mod = N1.*W1; 
W3mod = N3.*W3;

%% --------------------------- mixsyn -------------------------------------- %%

opts = hinfsynOptions('Method','LMI','Display','on','Autoscale','on');
[K_sc,CL,GAM] = mixsyn(P,W1mod,W2,W3mod,opts);
GAM

%% ---------------------- loop shape check --------------------------------- %%
P_SC = P_begin;

check_weighting_fnc_shape
%% ---------------------requirements check --------------------------------- %%

close all 
opts = bodeoptions('cstprefs'); 
opts.FreqUnits = 'Hz';
opts.MagUnits = 'abs';
opts.MagScale = 'log';

for i = 1:3 
    T_dist(i) = minreal(tf(s));                    % as derived at the begin
    T_noise(i) = minreal(tf(s));
    figure (i)
    bodemag(T_dist,fp,opts),hold on
    bodemag(T_noise,fp,opts),hold on
    bodemag(B80,fp,'r'),grid on
    legend
end 

return

%% --------------------- Order Reduction --------------------------------- %%

for i = 1:3
    hsv = hankelsv(K_sc(i,i));
    figure
    semilogy(hsv,'*--')
end

opt = balredOptions('StateElimMethod','MatchDC');
Ksc_red = balred(K_sc,14,opt);

for i = 1:3
    for j = 1:3
        Ksc_mod(i,j) = minreal(Ksc_red(i,j));
        figure
        bodemag(Ksc_mod(i,j),Ksc_red(i,j),'k --')
        hold on
        bodemag(K_sc(i,j),'r*')
        legend
    end
end


%% ---------------------requirements check --------------------------------- %%

Plant(:,:) = tf(LISA_plant_end(1:3,4:6));

opts = bodeoptions('cstprefs'); 
opts.FreqUnits = 'Hz';
opts.MagUnits = 'abs';
opts.MagScale = 'log';

for i = 1:3
    TFu = minreal(with the reducted controller; 
    TFy = minreal(with the reducted controller);
    figure
    bodemag(T_dist,fp,opts),hold on
    bodemag(T_noise,fp,opts),hold on
    bodemag(B80,fp),grid on
    legend ('T_dist','T_noise')
    title('Ksc ridotto')
end 

%%
for i = 1:3
    T_dist = minreal(with Ksc_mod); 
    T_noise = minreal(with Ksc_mod);
    figure
    bodemag(T_dist,fp,opts),hold on
    bodemag(T_noise,fp,opts),hold on
    bodemag(B80,fp),grid on
    legend ('T_dist','T_noise')
    title('Ksc mod')
end 

return
%%
save('K_mimo','Ksc_red','-append')
save('K_mimo','Ksc_mod','-append')
