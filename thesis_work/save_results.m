%% saving results for each random simulation

dir = '../MONTE_CARLO/Monte_Carlo_Results/Results/';
root_sim_file_name = 'results_sim_num';
iter_str = num2str(iter);
results_sim_file_name = strcat('results_sim_num',num2str(iter));

file_path = strcat(dir,results_sim_file_name);

save(file_path,'aI_M1')
save(file_path,'aI_M2','-append')
save(file_path,'aI_M1_x','-append')
save(file_path,'aI_M2_x','-append')
save(file_path,'r_M1','-append')
save(file_path,'r_M2','-append')
save(file_path,'theta_M1','-append')
save(file_path,'theta_M2','-append')
save(file_path,'theta_SC','-append')
save(file_path,'e_OA','-append')
save(file_path,'F_T','-append')
save(file_path,'M_T','-append')
save(file_path,'F_E1','-append')
save(file_path,'F_E2','-append')
save(file_path,'M_E1','-append')
save(file_path,'M_E2','-append')
save(file_path,'tout','-append')

