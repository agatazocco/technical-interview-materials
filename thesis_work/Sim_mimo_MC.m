%%%%% MONTE CAROLO SIMULATIONS %%%%%%%	

clear all 
close all 
clc 

main_LISA_hinf_MC
Tfin = 300000; 					%simulation time

N = 50;							%number of simulation

for iter = 1:N
    MC_init						%to randomly initialize the parameters at each iteration
    set_param('MC_mimo','SimulationMode','Accelerator')		%to set the accelerator mode in simulink (it generates an executable of the simulink model)
    sim ('MC_mimo')
    save_results
iter
end

%% RESULTS EVALUATION %%
cd ../MONTE_CARLO/Monte_Carlo_Results/
mc_Results
